<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180417102125 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE message (id INT AUTO_INCREMENT NOT NULL, file_id INT DEFAULT NULL, message VARCHAR(255) NOT NULL, source VARCHAR(255) DEFAULT NULL, severity INT DEFAULT NULL, type VARCHAR(255) NOT NULL, line_num INT DEFAULT NULL, column_num INT DEFAULT NULL, fixable TINYINT(1) DEFAULT NULL, INDEX IDX_B6BD307F93CB796C (file_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE file (id INT AUTO_INCREMENT NOT NULL, commit_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, errors_count INT NOT NULL, warnings_count INT NOT NULL, fixable_count INT NOT NULL, INDEX IDX_8C9F36103D5814AC (commit_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE commit (id INT AUTO_INCREMENT NOT NULL, project_id INT DEFAULT NULL, author_id INT DEFAULT NULL, hash VARCHAR(255) NOT NULL, date DATETIME NOT NULL, INDEX IDX_4ED42EAD166D1F9C (project_id), INDEX IDX_4ED42EADF675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE project (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F93CB796C FOREIGN KEY (file_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE file ADD CONSTRAINT FK_8C9F36103D5814AC FOREIGN KEY (commit_id) REFERENCES commit (id)');
        $this->addSql('ALTER TABLE commit ADD CONSTRAINT FK_4ED42EAD166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE commit ADD CONSTRAINT FK_4ED42EADF675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE commit DROP FOREIGN KEY FK_4ED42EADF675F31B');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307F93CB796C');
        $this->addSql('ALTER TABLE file DROP FOREIGN KEY FK_8C9F36103D5814AC');
        $this->addSql('ALTER TABLE commit DROP FOREIGN KEY FK_4ED42EAD166D1F9C');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE message');
        $this->addSql('DROP TABLE file');
        $this->addSql('DROP TABLE commit');
        $this->addSql('DROP TABLE project');
    }
}
