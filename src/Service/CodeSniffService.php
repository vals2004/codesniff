<?php
namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ODM\MongoDB\DocumentManager;
use App\Entity\Project;
use App\Entity\User;
use App\Entity\Commit;
use App\Entity\File;
use App\Entity\Message;
use App\Document\Commit as Document;

class CodeSniffService
{
    /**
     * @var 
     */
    private $em;

    public function __construct(EntityManagerInterface $entityManager, DocumentManager $documentManager)
    {
        $this->em = $entityManager;
        $this->dm = $documentManager;
    }

    public function post($data)
    {
        $project = $this->em->getRepository('App:Project')->findOneBy(['name'=>$data->project]);
        if (!$project) {
            $project = new Project;
            $project->setName($data->project);
            $this->em->persist($project);
        }

        $author= $this->em->getRepository('App:User')->findOneBy(['email'=>$data->email]);
        if (!$author) {
            $author= new User;
            $author->setName($data->author);
            $author->setEmail($data->email);
            $this->em->persist($author);
        }

        $commit = new Commit;
        $commit->setHash($data->hash);
        $commit->setBranch($data->branch);
        $commit->setDate(new \DateTime($data->date));
        $commit->setProject($project);
        $commit->setAuthor($author);
        $this->em->persist($commit);

        foreach ($data->files as $fileName=>$fileData) {
            $file = new File;
            $file->setName($fileName);
            $file->setErrorsCount($fileData->totals->errors);
            $file->setWarningsCount($fileData->totals->warnings);
            $file->setFixableCount($fileData->totals->fixable);
            $file->setCommit($commit);
            $this->em->persist($file);

            foreach ($fileData->files->STDIN->messages as $messageData) {
                $message = new Message;
                $message->setMessage($messageData->message);
                $message->setSource($messageData->source);
                $message->setSeverity($messageData->severity);
                $message->setType($messageData->type);
                $message->setLine($messageData->line);
                $message->setColumn($messageData->column);
                $message->setFixable($messageData->fixable);
                $message->setFile($file);
                $this->em->persist($message);
            }
        }

        $this->em->flush();

        $document = new Document;
        $document->setProject($data->project);
        $document->setBranch($data->branch);
        $document->setHash($data->hash);
        $document->setDate(new \DateTime($data->date));
        $document->setAuthor($data->author);
        $document->setEmail($data->email);
        $document->setData($data->files);

        $this->dm->persist($document);
        $this->dm->flush();
    }
}
