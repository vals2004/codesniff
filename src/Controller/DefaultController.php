<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\CodeSniffService;

class DefaultController extends Controller
{
    /**
     * @Route("/post-receive", name="post_receive", methods={"POST"})
     */
    public function postReceiveAction(Request $request, CodeSniffService $cs)
    {
        $data = json_decode($request->getContent());

        $cs->post($data);

        $response = new JsonResponse();         

        return $response;
    }
}
