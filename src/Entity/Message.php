<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MessageRepository")
 */
class Message
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $message;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $source;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $severity;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $line_num;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $column_num;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $fixable;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\File", inversedBy="messages")
     */
    private $file;

    public function getId()
    {
        return $this->id;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(string $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function getSeverity(): ?int
    {
        return $this->severity;
    }

    public function setSeverity(int $severity): self
    {
        $this->severity = $severity;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getLine(): ?int
    {
        return $this->line_num;
    }

    public function setLine(?int $line): self
    {
        $this->line_num = $line;

        return $this;
    }

    public function getColumn(): ?int
    {
        return $this->column_num;
    }

    public function setColumn(?int $col): self
    {
        $this->column_num = $col;

        return $this;
    }

    public function getFixable(): ?bool
    {
        return $this->fixable;
    }

    public function setFixable(bool $fixable): self
    {
        $this->fixable = $fixable;

        return $this;
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function setFile(File $file): self
    {
        $this->file = $file;

        return $this;
    }
}
