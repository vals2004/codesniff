<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FileRepository")
 */
class File
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $errors_count;

    /**
     * @ORM\Column(type="integer")
     */
    private $warnings_count;

    /**
     * @ORM\Column(type="integer")
     */
    private $fixable_count;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Commit", inversedBy="files")
     */
    private $commit;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Message", mappedBy="file")
     */
    private $messages;

    public function __construct()
    {
        $this->messages = new ArrayCollection();
    } 

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getErrorsCount(): ?int
    {
        return $this->errors_count;
    }

    public function setErrorsCount(int $errors): self
    {
        $this->errors_count = $errors;

        return $this;
    }

    public function getWarningsCount(): ?int
    {
        return $this->warnings_count;
    }

    public function setWarningsCount(int $warnings): self
    {
        $this->warnings_count = $warnings;

        return $this;
    }

    public function getFixableCount(): ?int
    {
        return $this->fixable_count;
    }

    public function setFixableCount(int $fixable): self
    {
        $this->fixable_count = $fixable;

        return $this;
    }

    public function getCommit(): ?Commit
    {
        return $this->commit;
    }

    public function setCommit(Commit $commit): self
    {
        $this->commit = $commit;

        return $this;
    }

    public function getMessages(): ?ArrayCollection
    {
        return $this->messages;
    }
}
