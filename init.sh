#!/bin/sh

read_param () 
{
    DEFAULT=$1
    RESULT=$DEFAULT

    read VALUE

    if [ "$VALUE" != "" ]; then
        RESULT=$VALUE
    fi

    echo $RESULT
}


USER=`whoami`
UUID=`cat /etc/passwd| grep ${USER} |awk -F: '{print $3}'`
HTTP_PORT=1080
HTTPS_PORT=1443
MYSQL_PORT=1306
XDEBUG_CLIENT_IP=172.18.0.1
echo -n "UID [$UUID]: "
UUID=`read_param $UUID`
echo -n "HTTP_PORT [$HTTP_PORT]: "
HTTP_PORT=`read_param $HTTP_PORT`
echo -n "HTTPS_PORT [$HTTPS_PORT]: "
HTTPS_PORT=`read_param $HTTPS_PORT`
echo -n "MYSQL_PORT [$MYSQL_PORT]: "
MYSQL_PORT=`read_param $MYSQL_PORT`
echo -n "XDEBUG_CLIENT_IP [$XDEBUG_CLIENT_IP]: "
XDEBUG_CLIENT_IP=`read_param $XDEBUG_CLIENT_IP`
cp .env.dist .env
sed -i "s/HOST_UID=.*/HOST_UID=$UUID/" .env
sed -i "s/HTTP_PORT=.*/HTTP_PORT=$HTTP_PORT/" .env
sed -i "s/HTTPS_PORT=.*/HTTPS_PORT=$HTTPS_PORT/" .env
sed -i "s/MYSQL_PORT=.*/MYSQL_PORT=$MYSQL_PORT/" .env
sed -i "s/XDEBUG_CLIENT_IP=.*/XDEBUG_CLIENT_IP=$XDEBUG_CLIENT_IP/" .env

docker-compose build
docker-compose up -d

docker-compose exec web sudo -u www-data composer install
docker-compose exec web sudo -u www-data composer require encore
docker-compose exec web sudo -u www-data yarn install
docker-compose exec web sudo -u www-data yarn add sass-loader node-sass --dev
docker-compose down
